## 1. Введение

### 1.1 Цель работы

Цель данной работы — разработка структуры данных для хранения множеств с использованием битовых полей, а также освоение таких инструментов разработки программного обеспечения, как система контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

### 1.2 Краткое описание работы

В языке программирования С++ уже имеется такой тип данных, как структура с битовыми полями, однако работа с ним крайне неудобна. Ведь не получится использовать стандартные побитовые операции между двумя объектами такого типа, а также трудно реализовать удобный доступ к элементам битового поля. Поэтому идеальным решение будет использование возможностей ООП. В качестве элемента хранящего в себе биты, будет использован *unsigned int*, так как он наиболее простым способом представляется в памяти, а поразрядные операции языка С++ помогут нам реализовать основные методы будущего класса.

### 1.3 Исключения, возникающие в ходе программы

Ниже перечислены исключения, которые могут быть вызваны при выполнении программы с использованием классов **TBitField** и **TSet**.

- 1 - Попытка создания объекта с отрицательным размером.
- 2 - Не выделилась память для объекта.
- 3 - Указан некорректный индекс при работе с отдельным битом (элементом множества)

## 2. Код программы и тесты

### 2.1 Код реализации класса TBitField

    #include "tbitfield.h"
    #include <string>
    
    TBitField::TBitField(int len)
    {
    	BitLen = len;
    
    	if (len < 0)
    		throw 1;
    	if (len % (sizeof(TELEM) * 8) == 0)
    		MemLen = len / (sizeof(TELEM) * 8);
    	else
    		MemLen = len / (sizeof(TELEM) * 8) + 1;
    
    	pMem = new TELEM[MemLen];
    
    	if (pMem == nullptr)
    		throw 2;
    
    	for (int i = 0; i < MemLen; i++)
    		pMem[i] = 0;
    }
    
    TBitField::TBitField(const TBitField &bf) // конструктор копирования
    {
    	BitLen = bf.BitLen;
    	MemLen = bf.MemLen;
    
    	pMem = new TELEM[MemLen];
    
    	if (pMem == nullptr)
    		throw 2;
    
    	for (int i = 0; i < MemLen; i++)
    		pMem[i] = bf.pMem[i];
    }
    
    TBitField::~TBitField()
    {
    	delete[] pMem;
    }
    
    int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
    {
    	if ((n + 1) % (sizeof(TELEM) * 8) == 0)
    		return (n + 1) / (sizeof(TELEM) * 8) - 1;
    	else
    		return (n + 1) / (sizeof(TELEM) * 8);
    }
    
    TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
    {
    	TELEM temp = 1;
    	int shift = n % (sizeof(TELEM) * 8);
    
    	temp = temp << shift;
    
    	return temp;
    }
    
    // доступ к битам битового поля
    
    int TBitField::GetLength(void) const // получить длину (к-во битов)
    {
    	return BitLen;
    }
    
    void TBitField::SetBit(const int n) // установить бит
    {
    	if (n >= 0 && n < BitLen)
    		pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | GetMemMask(n);
    	else
    		throw 3;
    }	
    
    void TBitField::ClrBit(const int n) // очистить бит
    {
    	if (n >= 0 && n < BitLen)
    		pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] & ~GetMemMask(n);
    	else
    		throw 3;
    }
    
    int TBitField::GetBit(const int n) const // получить значение бита
    {
    	if (n >= 0 && n < BitLen)
    		return pMem[GetMemIndex(n)] & GetMemMask(n) ? 1 : 0;
    	else
    		throw 3;
    }
    
    // битовые операции
    
    TBitField& TBitField::operator=(const TBitField &bf) // присваивание
    {
    	if (this == &bf)
    		return *this;
    	else
    	{
    		BitLen = bf.BitLen;
    		MemLen = bf.MemLen;
    
    		delete[] pMem;
    
    		pMem = new TELEM[MemLen];
    
    		if (pMem == nullptr)
    			throw 2;
    
    		for (int i = 0; i < MemLen; i++)
    			pMem[i] = bf.pMem[i];
    
    		return *this;
    	}	
    }
    
    int TBitField::operator==(const TBitField &bf) const // сравнение
    {
    	if (BitLen != bf.BitLen)
    		return 0;
    	else
    	{
    		for (int i = 0; i < BitLen; i++)
    			if (GetBit(i) != bf.GetBit(i))
    				return 0;
    	}
    
    	return 1;
    }
    
    int TBitField::operator!=(const TBitField &bf) const // сравнение
    {
    	return !(*this == bf);
    }
    
    TBitField TBitField::operator|(const TBitField &bf) // операция "или"
    {
    	if (BitLen < bf.BitLen)
    	{
    		TBitField tmp(bf);
    		for (int i = 0; i < MemLen; i++)
    			tmp.pMem[i] = tmp.pMem[i] | pMem[i];
    		return tmp;
    	}
    	else
    	{
    		TBitField tmp(*this);
    		for (int i = 0; i < bf.MemLen; i++)
    			tmp.pMem[i] = tmp.pMem[i] | bf.pMem[i];
    		return tmp;
    	}
    }
    
    TBitField TBitField::operator&(const TBitField &bf) // операция "и"
    {
    	if (BitLen < bf.BitLen)
    	{
    		TBitField tmp(bf);
    		for (int i = 0; i < MemLen; i++)
    			tmp.pMem[i] = tmp.pMem[i] & pMem[i];
    		return tmp;
    	}
    	else
    	{
    		TBitField tmp(*this);
    		for (int i = 0; i < bf.MemLen; i++)
    			tmp.pMem[i] = tmp.pMem[i] & bf.pMem[i];
    		return tmp;
    	}
    }
    
    TBitField TBitField::operator~(void) // отрицание
    {
    	TBitField tmp(*this);
    
    	for (int i = 0; i < MemLen; i++)
    		tmp.pMem[i] = ~tmp.pMem[i];
    
    	return tmp;
    }
    
    // ввод/вывод
    
    istream &operator>>(istream &istr, TBitField &bf) // ввод
    {
    	string s;
    	istr >> s;
    
    	for (int i = 0; i < s.size(); i++)
    	{
    		if (s[i] == '0')
    			bf.ClrBit(i);
    		else
    			bf.SetBit(i);
    	}
    
    	return istr;
    }
    
    ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
    {
    	for (int i = 0; i < bf.BitLen; i++)
    		ostr << bf.GetBit(i);
    
    	return ostr;
    }

### 2.2 Код реализации класса TSet