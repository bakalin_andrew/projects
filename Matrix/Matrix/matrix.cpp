#include "matrix.h"

/*--------------------------
��������������� �������
--------------------------*/

void Swap(Row* &r1, Row* &r2)
{
	Row* temp = r1;
	r1 = r2;
	r2 = temp;
}

/*--------------------------
���������� ���������������� ������ Row
--------------------------*/

Row::Row(unsigned _size) : size(_size)
{
	if (size > 0)
	{
		data = new double[size];
		if (data == nullptr)
			throw 1;
	}
	else
		data = nullptr;
}

Row::Row(const Row& r) : size(r.size)
{
	if (size > 0)
	{
		data = new double[size];
		if (data == nullptr)
			throw 1;
		for (int i = 0; i < size; i++)
			(*this)[i] = r[i];
	}
	else
		data = nullptr;
}

Row Row::operator-(const Row& r) const
{
	Row temp = r;

	for (int i = 0; i < size; i++)
		temp[i] = (*this)[i] - r[i];

	return temp;
}

Row Row::operator*(double num) const
{
	Row temp = *this;

	for (int i = 0; i < size; i++)
		temp[i] *= num;

	return temp;
}

Row Row::operator/(double num) const
{
	Row temp = *this;

	if (num != 0)
		for (int i = 0; i < size; i++)
			temp[i] /= num;
	else
		throw 3;

	return temp;
}

const Row& Row::operator=(const Row& r)
{
	for (int i = 0; i < size; i++)
		(*this)[i] = r[i];

	return *this;
}

double& Row::operator[](unsigned i) const
{
	if (i < size)
		return data[i];
	else
		throw 4;
}

Row::~Row()
{
	delete []data;
}

/*--------------------------
���������� ������ Matrix
--------------------------*/

Matrix::Matrix(unsigned _N, unsigned _M) : N(_N), M(_M)
{
	if (N > 0 && M > 0)
	{
		data = new Row*[N];
		if (data == nullptr)
			throw 1;

		for (int i = 0; i < N; i++)
		{
			data[i] = new Row(M);
			if (data[i] == nullptr)
				throw 1;

			for (int j = 0; j < M; j++)
				(*this)[i][j] = 0;
		}
	}
	else
	{
		N = M = 0;
		data = nullptr;
	}
}

Matrix::Matrix(const Matrix& m) : N(m.N), M(m.M)
{
	if (N > 0 && M > 0)
	{
		data = new Row*[N];
		if (data == nullptr)
			throw 1;

		for (int i = 0; i < N; i++)
		{
			data[i] = new Row(M);
			if (data[i] == nullptr)
				throw 1;

			for (int j = 0; j < M; j++)
				(*this)[i][j] = m[i][j];
		}
	}
	else
		data = nullptr;
}

Matrix::~Matrix()
{
	for (int i = 0; i < N; i++)
		delete data[i];
			
		delete[]data;
}

Matrix Matrix::operator+(const Matrix& m) const
{
	Matrix temp(N, M);
	
	if (N == m.N && M == m.M)
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				temp[i][j] = (*this)[i][j] + m[i][j];
	else
		throw 2;

	return temp;
}

Matrix Matrix::operator-(const Matrix& m) const
{
	Matrix temp(N, M);

	if (N == m.N && M == m.M)
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				temp[i][j] = (*this)[i][j] - m[i][j];
	else
		throw 2;

	return temp;
}

Matrix Matrix::operator*(const Matrix& m) const
{
	Matrix temp(N, m.M);

	if (M == m.N)
		for (int i = 0; i < N; i++)
			for (int j = 0; j < m.M; j++)
			{
				temp[i][j] = 0;
				for (int k = 0; k < M; k++)
					temp[i][j] += (*this)[i][k] * m[k][j];
			}
	else
		throw 2;

	return temp;
}

Matrix Matrix::operator+(double num) const
{
	Matrix temp = *this;

	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			temp[i][j] += num;

	return temp;
}

Matrix Matrix::operator-(double num) const
{
	Matrix temp = *this;

	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			temp[i][j] -= num;

	return temp;
}

Matrix Matrix::operator*(double num) const
{
	Matrix temp = *this;

	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			temp[i][j] *= num;

	return temp;
}

Matrix Matrix::operator/(double num) const
{
	Matrix temp = *this;

	if (num != 0)
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				temp[i][j] /= num;
	else
		throw 3;

	return temp;
}

const Matrix& Matrix::operator=(const Matrix& m)
{
	if (this == &m)
		return *this;
	else
	{
		for (int i = 0; i < N; i++)
			delete data[i];

		delete[]data;

		N = m.N;
		M = m.M;

		if (N > 0 && M > 0)
		{
			data = new Row*[N];
			if (data == nullptr)
				throw 1;

			for (int i = 0; i < N; i++)
			{
				data[i] = new Row(M);
				if (data[i] == nullptr)
					throw 1;

				for (int j = 0; j < M; j++)
					(*this)[i][j] = m[i][j];
			}
		}
		else
			data = nullptr;

		return *this;
	}

}

Matrix& Matrix::operator+=(const Matrix& m)
{
	*this = *this + m;

	return *this;
}

Matrix& Matrix::operator-=(const Matrix& m)
{
	*this = *this - m;

	return *this;
}

Matrix& Matrix::operator*=(const Matrix& m)
{
	Matrix temp = *this * m;
	*this = temp;

	return *this;
}

Matrix& Matrix::operator+=(double num)
{
	*this = *this + num;

	return *this;
}

Matrix& Matrix::operator-=(double num)
{
	*this = *this - num;

	return *this;
}

Matrix& Matrix::operator*=(double num)
{
	*this = *this * num;

	return *this;
}

Matrix& Matrix::operator/=(double num)
{
	*this = *this / num;

	return *this;
}

bool Matrix::operator==(const Matrix& m) const
{
	if (N != m.N || M != m.M)
		return false;
	else
	{
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				if ((*this)[i][j] != m[i][j])
					return false;
	}

	return true;
}

bool Matrix::operator!=(const Matrix& m) const
{
	return !((*this) == m);
}

Row& Matrix::operator[](unsigned i)const
{
	if (i < N)
		return *data[i];
	else
		throw 4;
}

double Matrix::det() const
{
	Matrix temp = *this;
	int k;
	double d = 1;

	if (N != M || data == nullptr)
		throw 5;
	else
	{
		for (int i = 0; i < N - 1; i++)
		{
			if (temp[i][i] == 0)
			{
				k = i + 1;

				while (k < N && temp[k][i] == 0)
					k++;

				if (k == N)
					return 0;

				Swap(temp.data[i], temp.data[k]);
			}

			for (int j = i + 1; j < N; j++)
				if (temp[j][i] != 0)
					temp[j] = temp[j] - temp[i] * temp[j][i] / temp[i][i];
		}

		for (int i = 0; i < N; i++)
			d *= temp[i][i];

		return d;
	}
}

Matrix Matrix::inverse() const
{
	Matrix E(N, N), temp = *this;
	int k;

	if (det() == 0)
		throw 6;
	else
	{
		for (int i = 0; i < N; i++)
			E[i][i] = 1;

		for (int i = 0; i < N - 1; i++)
		{
			if (temp[i][i] == 0)
			{
				k = i + 1;

				while (k < N && temp[k][i] == 0)
					k++;

				if (k == N)
					return 0;

				Swap(temp.data[i], temp.data[k]);
				Swap(E.data[i], E.data[k]);
			}

			E[i] = E[i] / temp[i][i];
			temp[i] = temp[i] / temp[i][i];

			for (int j = i + 1; j < N; j++)
				if (temp[j][i] != 0)
				{
					E[j] = E[j] - E[i] * temp[j][i];
					temp[j] = temp[j] - temp[i] * temp[j][i];
				}
		}
		
		E[N - 1] = E[N - 1] / temp[N - 1][N - 1];
		temp[N - 1] = temp[N - 1] / temp[N - 1][N - 1];

		for (int i = 0; i < N - 1; i++)
		{
			for (int j = i + 1; j < N; j++)
			{
				E[i] = E[i] - E[j] * temp[i][j];
				temp[i] = temp[i] - temp[j] * temp[i][j];
			}
		}

		return E;
	}
}

Matrix Matrix::transpose() const
{
	Matrix temp(M, N);

	for (int i = 0; i < M; i++)
		for (int j = 0; j < N; j++)
			temp[i][j] = (*this)[j][i];

	return temp;
}

void Matrix::fill(unsigned max)
{
	srand(time(0));

	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			(*this)[i][j] = rand() % (max + 1);
}

void Matrix::save(char a[]) const
{
	std::ofstream fout(a, std::ios_base::out);

	if (!fout.is_open())
		throw 7;

	fout << N << ' ' << M << '\n';

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
			fout << (*this)[i][j] << ' ';
		fout << '\n';
	}
}

/*--------------------------
������� �������� ������
--------------------------*/

Matrix load(char a[])
{
	unsigned _N, _M;

	std::ifstream fin(a, std::ios_base::in);
	
	if (!fin.is_open())
		throw 8;

	fin >> _N >> _M;

	Matrix temp(_N, _M);

	for (int i = 0; i < _N; i++)
		for (int j = 0; j < _M; j++)
			fin >> temp[i][j];

	fin.close();

	return temp;
}

/*--------------------------
���������� ������� �������
--------------------------*/

Matrix operator+(double num, const Matrix& m)
{
	return m + num;
}

Matrix operator*(double num, const Matrix& m)
{
	return m * num;
}

std::ostream& operator<<(std::ostream& os, const Matrix& v)
{
	os.setf(std::ios::fixed);

	for (int i = 0; i < v.N; i++)
	{
		os << std::setprecision(3) << v[i][0] << ' ';

		for (int j = 1; j < v.M; j++)
		{
			os << std::setw(10) << std::setprecision(3) << v[i][j] << ' ';
		}
		os << '\n';
	}

	return os;
}