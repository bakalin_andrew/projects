#include "matrix.h"
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	Matrix m1(2,2), m2(2,2), m3(3,3);

	m1[0][0] = -2 / sqrt(5);
	m1[0][1] = -1 / sqrt(5);
	m1[1][0] = -1 / sqrt(5);
	m1[1][1] = 2 / sqrt(5);

	cout << "m1\n" << m1;

	cout << "Result of m1.transpose()\n"<< m1.transpose() << "Result of m1.inverse()\n" << m1.inverse(); //Матрица m1 ортогональная

	m2[0][0] = 2;
	m2[0][1] = 1;
	m2[1][0] = 1;
	m2[1][1] = 2;

	cout << "m2\n" << m2;

	if (m2 == m2.transpose())
		cout << "m2 is symmetric\n";

	cout << "Result of addition and multiplication\n"<< m1 + m2 << m1 * m2;

	m3.fill();

	cout << "m3\n" << m3;

	cout << "" << m3.det();

	m3.save("m3.txt");
	
	return 0;
}
