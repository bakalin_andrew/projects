#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>

class Row
{
	protected:
		double *data;
		unsigned size;

	public:
		Row(unsigned = 0);

		Row(const Row&);

		Row operator-(const Row&) const; 

		Row operator*(double) const;

		Row operator/(double) const;

		const Row& operator=(const Row&);

		double& operator[](unsigned) const; //�������� ������� � �������� ������

		~Row();
};

/*--------------------------
�������� ������ Matrix
--------------------------*/

class Matrix
{
	friend std::ostream& operator<<(std::ostream&, const Matrix&);
	friend Matrix operator+(double, const Matrix&);
	friend Matrix operator*(double, const Matrix&);

	protected:
		Row **data;
		unsigned N, M;

	public:
		Matrix(unsigned = 0, unsigned = 0); //����������� � �����������
		
		Matrix(const Matrix&); //����������� �����������

		~Matrix(); //����������

		Matrix operator+(const Matrix&) const; //�������� �������� ���� ������

		Matrix operator-(const Matrix&) const; //�������� ��������� ���� ������

		Matrix operator*(const Matrix&) const; //�������� ��������� ���� ������

		Matrix operator+(double) const; //�������� �������� ������� � �����

		Matrix operator-(double) const; //�������� ��������� ������� � �����

		Matrix operator*(double) const; //�������� ��������� ������� � �����

		Matrix operator/(double) const; //�������� ������� ������� �� �����

		const Matrix& operator=(const Matrix&); //�������� ������������

		Matrix& operator+=(const Matrix&); //�������� ���������-����� � ����� ���������

		Matrix& operator-=(const Matrix&); //�������� �������-����� � ����� ���������

		Matrix& operator*=(const Matrix&); //�������� ��������-����� � ����� ���������

		Matrix& operator+=(double); //�������� ���������-����� � �������� � ������

		Matrix& operator-=(double); //�������� �������-����� � �������� � ������

		Matrix& operator*=(double); //�������� ��������-����� � �������� � ������

		Matrix& operator/=(double); //�������� ������-����� � �������� � ������

		bool operator==(const Matrix&) const; //�������� ���������

		bool operator!=(const Matrix&) const; //�������� �����������

		Row& operator[](unsigned) const; //�������� ������� � ������

		double det() const; //����� ���������� ������������

		Matrix inverse() const; //����� �������� �������� ������� (���� �������� ��������)

		Matrix transpose() const; //����� �������� ����������������� �������

		void fill(unsigned = 100); //����� ������������� ��������� �������

		void save(char[] = "output.txt") const; //����� ���������� ������ � ����� (�� ��������� output.txt)
};

/*--------------------------
������� �������� ������
--------------------------*/

Matrix load(char[] = "input.txt"); //������� �������� �� ����� (�� ��������� input.txt) 