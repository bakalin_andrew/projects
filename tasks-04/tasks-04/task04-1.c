#include <stdio.h>
#include <locale.h>

#define MIN 2
#define MAX 1000000

typedef long long int lli;

int length_of_seq(lli num)
{
	int length = 0;

	while (num != 1)
	{
		if (num % 2 == 0)
			num /= 2;
		else
			num = num * 3 + 1;

		length++;
	}

	return length;
}

int main()
{
	int max_length = 0, cur_length;
	lli i, res_num;

	setlocale(LC_ALL, "rus");

	for (i = MIN; i <= MAX; i++)
	{
		cur_length = length_of_seq(i);
		if (cur_length > max_length)
		{
			max_length = cur_length;
			res_num = i;
		}
	}

	printf("�����, ����������� ����� ������� ������������������ � �������� �� %d �� %d - ��� %lld\n", MIN, MAX, res_num);

	return 0;
}