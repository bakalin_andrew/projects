#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define N 256
#define MAX 256

void rand_words(char arr[])
{
	char words[N][MAX];
	char *p[N];
	int i, j, cur_word = 0, cur_letter = 0, on_word = 0, word_num;

	for (i = 0; i <= strlen(arr); i++)
	{
		if ((arr[i] == ' ') || (arr[i] == 0))
		{
			if (on_word == 1)
			{
				words[cur_word][cur_letter] = 0;
				cur_word++;
				cur_letter = 0;
			}
			on_word = 0;
		}
		else
		{
			words[cur_word][cur_letter++] = arr[i];
			on_word = 1;
		}
	}
	word_num = cur_word - 1;

	for (i = 0; i <= word_num; i++)
		p[i] = words[i];

	*arr = 0;

	for (i = word_num; i >= 1; i--)
	{
		cur_word = rand() % (i + 1);
		strcat(arr, p[cur_word]);
		strcat(arr, " ");

		if (cur_word != i)
			for (j = cur_word; j < i; j++)
				p[j] = p[j + 1];

	}
	strcat(arr, p[0]); strcat(arr, "\0");
}

int main()
{
	FILE *fin;
	int i, j, cur_line, lines = 0;
	char txt[N][MAX];
	char *p[N];

	fin = fopen("input.txt", "rt");
	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	if (fin == NULL)
	{
		perror("File");
	}

	while (fgets(txt[lines], MAX, fin))
	{
		if (txt[lines][strlen(txt[lines]) - 1] == '\n')
			txt[lines][strlen(txt[lines]) - 1] = 0;

		lines++;
	}

	lines--;

	fclose(fin);
	fin = fopen("output.txt", "wt");

	for (i = 0; i <= lines; i++)
		p[i] = txt[i];

	if (lines >= 0)
	{
		puts("������ � ������������� ������� �������� � ���� output.txt");

		for (i = 0; i <= lines; i++)
		{
			rand_words(p[i]);
			fprintf(fin, "%s\n", p[i]);
		}
	}
	else
	{
		puts("� �������� ����� �� ����� ������ �� ����������.");
	}

	fclose(fin);

	return 0;
}