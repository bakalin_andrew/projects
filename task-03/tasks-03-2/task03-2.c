#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define N 5
#define MAX 256

int main()
{
	int i, lines = 0;
	char txt[N][MAX];

	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	printf("������� ��������� ����� (����������� %d). ������ ������ ����� �������� ��������� �����.\n", N);

	while (1)
	{
		fgets(txt[lines], MAX, stdin);
		txt[lines][strlen(txt[lines]) - 1] = 0;

		if ((*txt[lines] == 0) || (lines == N - 1))
			break;

		lines++;
	}

	if (*txt[lines] == 0)
		lines--;

	if (lines >= 0)
	{
		puts("��������� ������ ����� ��������� ����:");
		puts(txt[rand() % (lines + 1)]);
	}
	else
	{
		puts("�� ����� ������ ������� �� ����.");
	}

	return 0;
}