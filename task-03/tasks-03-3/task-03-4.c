#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define N 5
#define MAX 256

int main()
{
	int i, j, cur_line, lines = 0;
	char txt[N][MAX];
	char *p[N];

	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	printf("������� ��������� ����� (����������� %d). ������ ������ ����� �������� ��������� �����.\n", N);

	while (1)
	{
		fgets(txt[lines], MAX, stdin);
		txt[lines][strlen(txt[lines]) - 1] = 0;

		if ((*txt[lines] == 0) || (lines == N - 1))
			break;

		lines++;
	}

	if (*txt[lines] == 0)
		lines--;

	for (i = 0; i <= lines; i++)
		p[i] = txt[i];

	if (lines >= 0)
	{
		puts("��������� ���� ������ � ��������� �������:");
		
		for (i = lines; i >= 1; i--)
		{
			cur_line = rand() % (i + 1);
			puts(p[cur_line]);

			for (j = cur_line; j < i; j++)
				p[j] = p[j + 1];
		}

		puts(p[0]);
	}
	else
	{
		puts("�� ����� ������ ������� �� ����.");
	}

	return 0;
}