//��� ���������� ����� � ������ ������������ �������

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <string.h>

typedef struct BOOK pBook;

struct BOOK {
	char title[40];
	char author[30];
	unsigned int year;
};

void get_book(char a[], pBook *b)
{
	int i = 0, j = 0;
	char cur_year[20];

	while (a[i] != ',')
		b->title[j++] = a[i++];
	b->title[j] = 0;
	j = 0; i++;

	while (a[i] != ',')
		b->author[j++] = a[i++];
	b->author[j] = 0;
	j = 0; i++;

	while (a[i] != 0)
		cur_year[j++] = a[i++];
	cur_year[j] = 0;

	b->year = atoi(cur_year);
}

int main()
{
	FILE *fin;
	pBook *arr, max_y, min_y, tmp;
	fpos_t position;
	char cur_book[128];
	int i, j,max_year, min_year, num_of_books = 0;

	setlocale(LC_ALL, "rus");

	//�������� ����� � ������ �������	
	fin = fopen("input.txt", "r+");

	if (fin == NULL)
		perror("File");
	
	fgetpos(fin, &position);

	while (fgets(cur_book, 128, fin))
		num_of_books++;

	fsetpos(fin, &position);

	arr = (pBook*)malloc(num_of_books*sizeof(pBook));

	for (i = 0; i < num_of_books; i++)
	{
		fgets(cur_book, 128, fin);
		get_book(cur_book, &arr[i]);
	}
	
	puts("��� ���������� � ������:");
	for (i = 0; i < num_of_books; i++)
		printf("\"%s\" %s, %d\n", arr[i].title, arr[i].author, arr[i].year);

	//����� ����� ����� � ����� ������ �����
	max_year = min_year = arr[0].year;
	max_y = min_y = arr[0];

	for (i = 1; i < num_of_books; i++)
		if (arr[i].year > max_year)
		{
			max_year = arr[i].year;
			max_y = arr[i];
		}
		else if (arr[i].year < min_year)
		{
			min_year = arr[i].year;
			min_y = arr[i];
		}

	//���������� �� ������ �������
	for (i = 0; i < num_of_books - 1; i++)
		for (j = 0; j < num_of_books - i - 1; j++)
			if (strcmp(arr[j].author, arr[j + 1].author) > 0)
			{
				tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
	
	
	puts("������ � ����� ����� (�� ���� �������) �����:");
	printf("\"%s\" %s, %d\n", max_y.title, max_y.author, max_y.year);
	puts("������ � ����� ������ (�� ���� �������) �����:");
	printf("\"%s\" %s, %d\n", min_y.title, min_y.author, min_y.year);
	puts("���������� � ������, ��������������� �� �������� �������");
	for (i = 0; i < num_of_books; i++)
		printf("\"%s\" %s, %d\n", arr[i].title, arr[i].author, arr[i].year);

	return 0;
}