#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <locale.h>

#define MAX 256

int main()
{
	FILE *fin;
	char str[MAX];
	int frequency[MAX];
	int i, onword = 0, num_of_sym = 0, num_of_digits = 0, num_of_marks = 0, num_of_words = 0, num_of_letters = 0, average_len;
	int max_in_freq = 0, popular_sym = 0;

	setlocale(LC_ALL, "rus");
	fin = fopen("input.txt", "rt");

	for (i = 0; i < MAX; i++)
		frequency[i] = 0;

	while (fgets(str, MAX, fin))
	{
		for (i = 0; i < strlen(str); i++)
		{
			num_of_sym++;

			if (str[i] >= '0' && str[i] <= '9')
				num_of_digits++;

			if (str[i] == '.' || str[i] == ',' || str[i] == '!' || str[i] == '?' || str[i] == ':' || str[i] == ';' || str[i] == '-')
				num_of_marks++;

			frequency[str[i]]++;

			if (str[i] >= 'a' && str[i] <= 'z' || str[i] >= 'A' && str[i] <= 'Z')
			{
				if (!onword)
				{
					onword = 1;
					num_of_words++;
				}
				num_of_letters++;
			}

			if (str[i] == ' ')
				onword = 0;
		}
	}
	
	for (i = 0; i < MAX; i++)
		if (frequency[i] > max_in_freq)
		{
			max_in_freq = frequency[i];
			popular_sym = i;
		}

	printf("���������� �������� � �����:\n%d\n", num_of_sym);
	printf("���������� ���� � �����:\n%d\n", num_of_words);
	printf("���������� ������ ���������� � �����:\n%d\n", num_of_marks);
	printf("���������� ���� � �����:\n%d\n", num_of_digits);
	printf("������� ����� ����� � �����:\n%d\n", num_of_letters / num_of_words);
	printf("����� ���������� ������ � �����:\n'%c'\n", popular_sym);

	fclose(fin);

	return 0;
}