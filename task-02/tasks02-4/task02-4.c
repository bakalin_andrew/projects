#include <stdio.h>
#include <locale.h>

#define N 3

int max_sum(int arr[][N], int S)
{
	int i, j, k, max_sum = 0, cur_sum, res_column = 0;

	for (i = 0; i < S; i++)
		max_sum += arr[i][0];

	for (j = 1; j < N; j++)
	{
		cur_sum = 0;

		for (i = 0; i < S; i++)
			cur_sum += arr[i][j];

		if (cur_sum > max_sum)
		{
			max_sum = cur_sum;
			res_column = j;
		}
	}

	return res_column + 1;
}


int main()
{
	setlocale(LC_ALL, "rus");

	int arr[N][N] = { { 1, 2, 3 },
					  { 1, 1, 3 },
					  { 2, 3, 3 } };
	int i, j;

	printf("������������ ����� ��������� ��������� � ������� ����� %d\n", max_sum(arr, N));

	return 0;
}