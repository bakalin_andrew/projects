#include <stdio.h>
#include <locale.h>

#define SIZE 16

int maxSeq(int *arr, int N)
{
	int i, count = 1, max = 1;
	for (i = 1; i < N; i++)
		if (arr[i] == arr[i - 1])
			count++;
		else
		{
			if (count > max)
				max = count;
			count = 1;
		}
	return max;
}

int main()
{
	setlocale(LC_ALL, "rus");

	int A[SIZE] = { 1, 1, 1, 1, 1, 2, 2, 2, 3, 2, 2, 2, 5, 5, 6, 7 };
	printf("����� ������� ������������������ ����� ����� � %d �������(��)\n", maxSeq(A, SIZE));
	return 0;
}