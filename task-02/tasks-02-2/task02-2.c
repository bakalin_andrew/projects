#include <stdio.h>
#include <string.h>
#include <locale.h>

int palindrome(char str[])
{
	int check = 1, len, i;
	len = strlen(str);
	
	for (i = 0; i < len / 2; i++)
		if (str[i] != str[len - i - 1])
		{
			check = 0;
			break;
		}
	return check;
}

int main()
{
	setlocale(LC_ALL, "rus");

	char str[256];

	puts("������� ������:");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = 0;

	if (palindrome(str))
		puts("��������� ������ �������� �����������!");
	else
		puts("��������� ������ �� �������� �����������!");

	return 0;
}